val commonSettings = Seq(
  scalaVersion := "2.12.10"
)

lazy val root = (project in file("."))
  .settings(commonSettings)
  .settings(
    name := "sky-media-movie"
  )
  .settings(libraryDependencies ++= Seq(
    "co.fs2" %% "fs2-io" % "2.3.0",
    "org.scalatest" %% "scalatest" % "3.1.2" % Test,
    "org.mockito" %% "mockito-scala" % "1.14.3" % Test)
  )
