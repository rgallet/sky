package sky.ratings

import sky.service.TechnicalFailure

sealed trait ParentalControlLevel {
  def level: Int
}

case object ParentalControlLevel {
  type PCL = ParentalControlLevel

  case object U extends ParentalControlLevel {
    val level: Int = 0
  }

  case object PG extends ParentalControlLevel {
    val level: Int = 5
  }

  case object _12 extends ParentalControlLevel {
    val level: Int = 10
  }

  case object _15 extends ParentalControlLevel {
    val level: Int = 15
  }

  case object _18 extends ParentalControlLevel {
    val level: Int = 20
  }

  def supersede(from: ParentalControlLevel, over: ParentalControlLevel): Boolean = from.level >= over.level // re-entrant, ie a PCL supersedes itself also

  implicit class PclDsl(pcl: ParentalControlLevel) {
    def supersede(over: ParentalControlLevel): Boolean = ParentalControlLevel.supersede(pcl, over)
  }

  def apply(s: String): ParentalControlLevel = s match {
    case "U" => U
    case "PG" => PG
    case "12" => _12
    case "15" => _15
    case "18" => _18
    case v => throw TechnicalFailure(new IllegalStateException(s"$v is not an available rating"))
  }
}