package sky.service

trait PCLResult

object PCLResult {

  case object Granted extends PCLResult // as in 'can watch'

  case object Denied extends PCLResult // as in 'can't watch'

  case class ResultError(e: Throwable) extends PCLResult // exception wrapper in this ADT

}

