package sky.service

import scala.util.{Failure, Success}

import cats.effect.Async
import com.thirdparty.movie.MovieService
import fs2._
import sky.ratings.ParentalControlLevel
import sky.ratings.ParentalControlLevel.PCL
import sky.service.PCLResult.{Denied, Granted, ResultError}
import sky.service.ParentalControlService.{AccessValidation, CheckAccessForMovieId}

/**
 * Declarative Control Flow with fs2 Stream.
 *
 * Given a customer's PCL, validates access to a movie, based on the movie's own rating.
 *
 * @see [[https://www.youtube.com/watch?v=YSN__0VEsaw]] for reference and concept
 *
 */
class ParentalControlService[F[_] : Async](movieService: MovieService) {
  def errorHandler: Throwable => fs2.Stream[F, PCLResult] = {
    case e: TechnicalFailure => fs2.Stream.emit(ResultError(e)).covary[F]
    case e: TitleNotFound => fs2.Stream.emit(ResultError(e)).covary[F]
    case e => fs2.Stream.emit(ResultError(TechnicalFailure(e))).covary[F]
  }

  /**
   * Validates access to a movie based on movie's PCL and validation rules in accessValidation.
   */
  def validatePCL(accessValidation: AccessValidation[F], movieId: String): F[PCLResult] =
    fs2.Stream.emit(movieId).covary[F]
      .evalMap { movieId =>
        implicitly[Async[F]].delay(movieService.getParentalControlLevel(movieId))
      }
      .flatMap {
        case Success(value) => fs2.Stream.emit(value).covary[F]
        case Failure(e: TitleNotFound) => fs2.Stream.raiseError[F](e)
        case Failure(e) => fs2.Stream.raiseError[F](TechnicalFailure(e))
      }
      .map(ParentalControlLevel.apply)
      .through(accessValidation)
      .handleErrorWith(errorHandler)
      .take(1)
      .compile
      .lastOrError

  /**
   * Creates an instance of CheckAccessForMovieId for a given customer's PCL.
   *
   * Applies default access validation rules, ie. U < PG < 12 < 15 < 18
   */
  def forCustomer(customerPCL: PCL): CheckAccessForMovieId[F] = {
    def hasAccess(customerPCL: PCL): AccessValidation[F] = _.map {
      case moviePCL if customerPCL supersede moviePCL => Granted
      case _ => Denied
    }

    validatePCL(hasAccess(customerPCL), _)
  }
}

object ParentalControlService {
  type CheckAccessForMovieId[F[_]] = String => F[PCLResult]
  type AccessValidation[F[_]] = Pipe[F, PCL, PCLResult]
}

