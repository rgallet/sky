package sky.service

import java.util.Objects

case class TechnicalFailure(exception: Throwable) extends RuntimeException(exception) {
  assert(Objects.nonNull(exception))

  override def getMessage: String = exception.getMessage
}
