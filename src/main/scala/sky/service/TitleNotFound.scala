package sky.service

case class TitleNotFound(movieId: String) extends RuntimeException(s"$movieId not found")
