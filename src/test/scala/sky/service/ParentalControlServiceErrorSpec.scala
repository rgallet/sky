package sky.service

import scala.util.{Failure, Success}

import cats.effect.IO
import com.thirdparty.movie.MovieService
import org.mockito.{ArgumentMatchersSugar, MockitoSugar}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import sky.ratings.ParentalControlLevel._
import sky.service.PCLResult.{Denied, Granted, ResultError}
import sky.service.ParentalControlService.AccessValidation

object ParentalControlServiceErrorSpec extends MockitoSugar {

  def withServices(f: (MovieService, ParentalControlService[IO]) => Unit): Unit = {
    val movieService = mock[MovieService]
    val parentalControlService = new ParentalControlService[IO](movieService)

    f(movieService, parentalControlService)
  }
}

class ParentalControlServiceErrorSpec extends AnyWordSpec with Matchers with MockitoSugar with ArgumentMatchersSugar {

  import ParentalControlServiceErrorSpec.withServices

  "ParentalControlService" when {
    "movie service returns null" should {
      "return a ResultError wrapping TechnicalFailure" in withServices { (movieService, parentalControlService) =>
        when(movieService.getParentalControlLevel(eqTo("movieId"))) thenReturn null
        val checkAccessForMovieId = parentalControlService.forCustomer(U)

        val result = checkAccessForMovieId("movieId").unsafeRunSync
        result shouldBe a[ResultError]
        result.asInstanceOf[ResultError].e shouldBe a[TechnicalFailure]
        result.asInstanceOf[ResultError].e.getCause shouldBe a[MatchError]
      }
    }

    "movie service throws an exception" should {
      "return a ResultError wrapping TechnicalFailure" in withServices { (movieService, parentalControlService) =>
        when(movieService.getParentalControlLevel(eqTo("movieId"))) thenThrow new IllegalAccessException("boom!")
        val checkAccessForMovieId = parentalControlService.forCustomer(U)

        val result = checkAccessForMovieId("movieId").unsafeRunSync
        result shouldBe a[ResultError]
        result.asInstanceOf[ResultError].e shouldBe a[TechnicalFailure]
        result.asInstanceOf[ResultError].e.getCause shouldBe a[IllegalAccessException]
      }
    }

    "movie service returns a Failure" should {
      "return a ResultError wrapping TechnicalFailure" in withServices { (movieService, parentalControlService) =>
        when(movieService.getParentalControlLevel(eqTo("movieId"))) thenReturn Failure(new IllegalAccessException("boom!"))
        val checkAccessForMovieId = parentalControlService.forCustomer(U)

        val result = checkAccessForMovieId("movieId").unsafeRunSync
        result shouldBe a[ResultError]
        result.asInstanceOf[ResultError].e shouldBe a[TechnicalFailure]
        result.asInstanceOf[ResultError].e.getCause shouldBe a[IllegalAccessException]
      }
    }

    "movie service returns TitleNotFound" should {
      "return a ResultError wrapping TitleNotFound" in withServices { (movieService, parentalControlService) =>
        when(movieService.getParentalControlLevel(eqTo("movieId"))) thenReturn Failure(TitleNotFound("movieId"))
        val checkAccessForMovieId = parentalControlService.forCustomer(U)

        val result = checkAccessForMovieId("movieId").unsafeRunSync
        result shouldBe a[ResultError]
        result.asInstanceOf[ResultError].e shouldBe a[TitleNotFound]
        result.asInstanceOf[ResultError].e.asInstanceOf[TitleNotFound].movieId shouldBe "movieId"
      }
    }
  }

  "error handler" when {
    "error is TechnicalFailure" should {
      "return a ResultError wrapping TechnicalFailure" in withServices { (_, parentalControlService) =>
        val failure = TechnicalFailure(new IllegalAccessException("boom!"))

        parentalControlService
          .errorHandler(failure)
          .compile
          .lastOrError
          .unsafeRunSync shouldBe ResultError(failure)
      }
    }

    "error is TitleNotFound" should {
      "return a ResultError wrapping TitleNotFound" in withServices { (_, parentalControlService) =>
        val failure = TitleNotFound("id")

        parentalControlService
          .errorHandler(failure)
          .compile
          .lastOrError
          .unsafeRunSync shouldBe ResultError(failure)
      }
    }

    "error is any other kind of exception" should {
      "return a ResultError wrapping TechnicalFailure" in withServices { (_, parentalControlService) =>
        val failure = new IllegalAccessException("boom!")

        parentalControlService
          .errorHandler(failure)
          .compile
          .lastOrError
          .unsafeRunSync shouldBe ResultError(TechnicalFailure(failure))
      }
    }
  }

  "validatePCL" when {
    "movie service return a valid PCL" should {
      "proceed with validation" in withServices { (movieService, parentalControlService) =>
        when(movieService.getParentalControlLevel(any[String])) thenReturn Success("U")

        def validation: AccessValidation[IO] = _.map(_ => Granted)

        parentalControlService
          .validatePCL(validation, "movieId")
          .unsafeRunSync shouldBe Granted
      }
    }

    "movie service return a invalid PCL" should {
      "result in a ResultError" in withServices { (movieService, parentalControlService) =>
        when(movieService.getParentalControlLevel(any[String])) thenReturn Success("dont know what you are")

        def notRelevantValidation: AccessValidation[IO] = _.map(_ => Denied)

        val result = parentalControlService
          .validatePCL(notRelevantValidation, "movieId")
          .unsafeRunSync

        result shouldBe a[ResultError]
        result.asInstanceOf[ResultError].e.getMessage shouldBe "dont know what you are is not an available rating"
      }
    }
  }
}
