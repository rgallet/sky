package sky.service

import scala.util.Success

import cats.effect.IO
import com.thirdparty.movie.MovieService
import org.mockito.{ArgumentMatchersSugar, MockitoSugar}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import sky.ratings.ParentalControlLevel._
import sky.service.PCLResult.{Denied, Granted}
import sky.service.ParentalControlService.CheckAccessForMovieId

object ParentalControlServiceAccessSpec extends MockitoSugar with ArgumentMatchersSugar{
  val MOVIE_ID = "movieId"

  def withLevel(pcl: PCL)(f: (MovieService, CheckAccessForMovieId[IO]) => Unit): Unit = {
    val movieService = mock[MovieService]
    val parentalControlService = new ParentalControlService[IO](movieService)
    val checkAccessForMovieId = parentalControlService.forCustomer(pcl)

    f(movieService, checkAccessForMovieId)
    verify(movieService) getParentalControlLevel (eqTo(MOVIE_ID))
    verifyNoMoreInteractions(movieService)
  }
}

class ParentalControlServiceAccessSpec extends AnyWordSpec with Matchers with MockitoSugar with ArgumentMatchersSugar {

  import ParentalControlServiceAccessSpec._

  "PCL level U" when {
    "checking access" should {
      "have access to U level movies" in withLevel(U) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("U")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "not have access to PG level movies" in withLevel(U) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("PG")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Denied)
      }

      "not have access to 12 level movies" in withLevel(U) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("12")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Denied)
      }

      "not have access to 15 level movies" in withLevel(U) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("15")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Denied)
      }

      "not have access to 18 level movies" in withLevel(U) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("18")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Denied)
      }
    }
  }

  "PCL level PG" when {
    "checking access" should {
      "have access to U level movies" in withLevel(PG) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("U")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "have access to PG level movies" in withLevel(PG) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("PG")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "not have access to 12 level movies" in withLevel(PG) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("12")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Denied)
      }

      "not have access to 15 level movies" in withLevel(PG) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("15")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Denied)
      }

      "not have access to 18 level movies" in withLevel(PG) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("18")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Denied)
      }
    }
  }

  "PCL level 12" when {
    "checking access" should {
      "have access to U level movies" in withLevel(_12) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("U")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "have access to PG level movies" in withLevel(_12) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("PG")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "have access to 12 level movies" in withLevel(_12) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("12")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "not have access to 15 level movies" in withLevel(_12) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("15")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Denied)
        verify(movieService) getParentalControlLevel (eqTo(MOVIE_ID))
        verifyNoMoreInteractions(movieService)
      }

      "not have access to 18 level movies" in withLevel(_12) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("18")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Denied)
      }
    }
  }

  "PCL level 15" when {
    "checking access" should {
      "have access to U level movies" in withLevel(_15) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("U")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "have access to PG level movies" in withLevel(_15) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("PG")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "have access to 12 level movies" in withLevel(_15) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("12")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "have access to 15 level movies" in withLevel(_15) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("15")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "not have access to 18 level movies" in withLevel(_15) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("18")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Denied)
      }
    }
  }

  "PCL level 18" when {
    "checking access" should {
      "have access to U level movies" in withLevel(_18) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("U")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "have access to PG level movies" in withLevel(_18) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("PG")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "have access to 12 level movies" in withLevel(_18) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("12")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "have access to 15 level movies" in withLevel(_18) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("15")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }

      "have access to 18 level movies" in withLevel(_18) { (movieService, checkAccessForMovieId) =>
        when(movieService.getParentalControlLevel(eqTo(MOVIE_ID))) thenReturn Success("18")
        checkAccessForMovieId(MOVIE_ID).unsafeRunSync should be(Granted)
      }
    }
  }
}
