package sky.ratings

import org.mockito.{ArgumentMatchersSugar, MockitoSugar}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import ParentalControlLevel._
import sky.service.TechnicalFailure

class ParentalControlLevelSpec extends AnyWordSpec with Matchers with MockitoSugar with ArgumentMatchersSugar {

  "Associated values" that {
    "are for each PCL" should {
      "be fixed" in {
        U.level shouldBe 0
        PG.level shouldBe 5
        _12.level shouldBe 10
        _15.level shouldBe 15
        _18.level shouldBe 20
      }
    }
  }

  "Constructing PCL from string" when {
    "U" should {
      "be U" in {
        ParentalControlLevel("U") shouldBe U
      }
    }

    "PG" should {
      "be PG" in {
        ParentalControlLevel("PG") shouldBe PG
      }
    }

    "12" should {
      "be _12" in {
        ParentalControlLevel("12") shouldBe _12
      }
    }

    "15" should {
      "be _15" in {
        ParentalControlLevel("15") shouldBe _15
      }
    }

    "18" should {
      "be _18" in {
        ParentalControlLevel("18") shouldBe _18
      }
    }

    "invalid string" should {
      "throw a TechnicalFailure" in {
        intercept[TechnicalFailure] {
          ParentalControlLevel("dsq")
        }

        intercept[TechnicalFailure] {
          ParentalControlLevel(null) //bouh...
        }
      }
    }
  }

  "Testing PCL ordering" when {
    "for U" should {
      "has order U < PG < 12 < 15 < 18" in {
        U supersede U shouldBe true
        U supersede PG shouldBe false
        U supersede _12 shouldBe false
        U supersede _15 shouldBe false
        U supersede _18 shouldBe false
      }
    }

    "for PG" should {
      "has order U < PG < 12 < 15 < 18" in {
        PG supersede U shouldBe true
        PG supersede PG shouldBe true
        PG supersede _12 shouldBe false
        PG supersede _15 shouldBe false
        PG supersede _18 shouldBe false
      }
    }

    "for _12" should {
      "has order U < PG < 12 < 15 < 18" in {
        _12 supersede U shouldBe true
        _12 supersede PG shouldBe true
        _12 supersede _12 shouldBe true
        _12 supersede _15 shouldBe false
        _12 supersede _18 shouldBe false
      }
    }

    "for _15" should {
      "has order U < PG < 12 < 15 < 18" in {
        _15 supersede U shouldBe true
        _15 supersede PG shouldBe true
        _15 supersede _12 shouldBe true
        _15 supersede _15 shouldBe true
        _15 supersede _18 shouldBe false
      }
    }

    "for _18" should {
      "has order U < PG < 12 < 15 < 18" in {
        _18 supersede U shouldBe true
        _18 supersede PG shouldBe true
        _18 supersede _12 shouldBe true
        _18 supersede _15 shouldBe true
        _18 supersede _18 shouldBe true
      }
    }
  }
}
